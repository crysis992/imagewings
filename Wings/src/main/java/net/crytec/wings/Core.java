package net.crytec.wings;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import com.google.common.collect.Maps;

import net.crytec.api.InventoryMenuAPI.InvGUI;
import net.crytec.api.InventoryMenuAPI.InvGUI.ROWS;
import net.crytec.api.InventoryMenuAPI.MenuEvent;
import net.crytec.api.itemstack.ItemBuilder;
import net.crytec.api.util.C;
import net.crytec.api.util.F;
import net.crytec.api.util.UtilMath;
import net.crytec.api.util.UtilTime;
import net.md_5.bungee.api.ChatColor;

public class Core extends JavaPlugin implements Listener {

	private HashMap<String, BufferedImage> images = Maps.newHashMap();
	private HashMap<UUID, BukkitTask> tasks = Maps.newHashMap();

	private HashMap<Player, Long> _lastMove = new HashMap<Player, Long>();

	@Override
	public void onEnable() {
		getDataFolder().mkdir();
		File f = new File(getDataFolder(), "wings");
		if (!f.exists()) {
			f.mkdir();
			this.saveResource("test.png", true);
			this.saveResource("cape.png", true);
			try {
			FileUtils.moveFile(new File(getDataFolder(), "test.png"), new File(f, "test.png"));
			FileUtils.moveFile(new File(getDataFolder(), "cape.png"), new File(f, "cape.png"));
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		this.loadImages();
		Bukkit.getPluginManager().registerEvents(this, this);
	}

	private void loadImages() {
		String[] ext = { "png" };
		File imagedir = new File(getDataFolder() + File.separator + "wings");
		Iterator<File> img = FileUtils.iterateFiles(imagedir, ext, false);

		while (img.hasNext()) {
			File file = img.next();
			try {
				BufferedImage image = ImageIO.read(file);
				images.put(file.getName().replaceAll(".png", ""), image);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void spawnParticle(Location loc, int r, int g, int b) {
		loc.getWorld().spawnParticle(Particle.REDSTONE, loc.getX(), loc.getY(), loc.getZ(), 0, getValue(r) + 0.0000001F, getValue(g), getValue(b), 1);
	}

	public float getValue(int input) {
		return (float) (1.0 / 255) * input;
	}

	public void spawnParticle(final int offsetl, int offsetr, final int offsety, final Player p, final int mode, Color color) {
		if (this.isMoving(p)) return;
		Location d = p.getLocation().clone();
		d.setPitch(0F);

		final Vector v = d.getDirection();

		final Vector vr = v.clone().setX(-v.getZ()).setZ(v.getX());

		final Vector vl = v.clone().setX(v.getZ()).setZ(-v.getX());

		final double ofr = ++offsetr * 0.3;
		final double ofl = offsetl * 0.3;
		final double ofy = offsety * 0.3;

		vr.multiply(ofr);
		vl.multiply(ofl * -1.0);

		double distance = 0.2d;
		double yawRadians = Math.PI * d.getYaw() / 180;
		d.add(distance * Math.sin(yawRadians), 0, -distance * Math.cos(yawRadians));

		if (mode == 0) {
			final Location left = d.add(vl.toLocation(p.getWorld()));
			left.add(0.0, ofy, 0.0);
			spawnParticle(left, color.getRed(), color.getGreen(), color.getBlue());
		} else {
			final Location right = d.add(vr.toLocation(p.getWorld()));
			right.add(0.0, ofy, 0.0);
			spawnParticle(right, color.getRed(), color.getGreen(), color.getBlue());
		}

	}

	public void parseFile(String name, Player p) {
		BufferedImage image = this.images.get(name);

		for (int yy = 0; yy < image.getHeight(); yy++) {

			for (int x = 0; x < image.getWidth(); x++) {

				final int clr = image.getRGB(x, yy);
				final int y = image.getHeight() - yy;
				Color color = new Color(clr);

				if (clr == 16777215) continue; // If it is transparent, skip
												// particle calculation/display.

				if (x < image.getWidth() / 2) {
					spawnParticle(x - image.getWidth() / 2, 0, y, p, 0, color);
				} else if (x > image.getWidth() / 2) {
					final int off = image.getWidth() - x;
					final int off2 = image.getWidth() / 2 - off;
					spawnParticle(0, off2, y, p, 1, color);
				}
			}
		}
	}

	@EventHandler
	public void closeOnQuit(PlayerQuitEvent e) {
		if (this.tasks.containsKey(e.getPlayer().getUniqueId())) {
			this.tasks.get(e.getPlayer().getUniqueId()).cancel();
			this.tasks.remove(e.getPlayer().getUniqueId());
		}
		this._lastMove.remove(e.getPlayer());
	}

	@EventHandler
	public void onWingMenuClick(MenuEvent e) {
		if (!e.getTitle().equals("Wings")) return;

		UUID id = e.getPlayer().getUniqueId();

		if (this.tasks.containsKey(id)) return;

		BukkitTask task = new BukkitRunnable() {

			@Override
			public void run() {
				parseFile(e.getItemName(), e.getPlayer());
			}
		}.runTaskTimerAsynchronously(this, 1, 5);

		this.tasks.put(id, task);
		e.getPlayer().sendMessage(F.main("Wings", "You've enabled your wings."));

	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!(sender instanceof Player)) {
			return true;
		}
		
		if (args.length == 1 && args[0].equalsIgnoreCase("reload")) {
			this.loadImages();
			sender.sendMessage(ChatColor.GREEN + "The plugin has been reloaded!");
			return true;
		}

		Player p = (Player) sender;
		
		if (!p.hasPermission("wings.use")) {
			p.sendMessage(ChatColor.RED + "You lack the proper permissions to use this command!");
			return true;
		}
		
		UUID id = p.getUniqueId();

		if (this.tasks.containsKey(id)) {
			this.tasks.get(id).cancel();
			this.tasks.remove(id);
			p.sendMessage(F.main("Wings", "Your wings are now disabled."));
			return true;
		}

		InvGUI gui = new InvGUI(p, "Wings", ROWS.ROW_2);

		for (String s : this.images.keySet()) {
			gui.addItem(new ItemBuilder(Material.PAPER).name(C.cGray + s).build());
		}

		gui.openInventory();

		return true;
	}

	@EventHandler
	public void setMoving(PlayerMoveEvent event) {
		if (!this.tasks.containsKey(event.getPlayer().getUniqueId())) return;
		if (UtilMath.offset(event.getFrom(), event.getTo()) <= 0.0D) {
			return;
		}
		_lastMove.put(event.getPlayer(), Long.valueOf(System.currentTimeMillis()));
	}

	public boolean isMoving(Player player) {
		if (!_lastMove.containsKey(player)) {
			return false;
		}
		return !UtilTime.elapsed(((Long) _lastMove.get(player)).longValue(), 500L);
	}

}
